#include "bitdecoded.h"

#include <set>
#include <sstream>
#include <iomanip>
#include <algorithm>

#include <QDebug>

#define MAX_WINDOW_WIDTH     890

namespace
{
inline void tokenize(const std::string& s, std::set<uint32_t>& sValue)
{
    const char* delimiters = " ,";
    char* data = strtok((char*) s.c_str(), delimiters);
    int i = 0;
    while(data != NULL)
    {
        sValue.insert(strtoul(data, NULL, 0));
        data = strtok(NULL, delimiters);
        ++i;
    }
}
}

BitDecoded::BitDecoded(QObject *parent) :
    QObject(parent), m_maxNrOfBits(32)
{

}

QString BitDecoded::inputValue()
{
    return m_inputValue;
}

void BitDecoded::setInputValue(const QString &inputValue)
{
    if (inputValue == m_inputValue)
        return;

    m_inputValue = inputValue;

    update();

    emit inputValueChanged();
}

void BitDecoded::setField(const QString& inputField)
{
    if (inputField == m_field)
    {
        return;
    }

    m_field = inputField;
    update();
    emit fieldChanged();
}

void BitDecoded::setWindowWidth(const uint windowWidth)
{
    if (m_windowWidth == windowWidth)
    {
        return;
    }

    m_windowWidth = windowWidth;
    update();
    emit windowWidthChanged();
}

void BitDecoded::pressButton()
{
    update();
    emit buttonPressed();
}

bool BitDecoded::inputStrToValue(qulonglong& inputValue, QString input)
{
    bool result = true;

    if (input.isEmpty())
    {
        return true;
    }

    if (input.startsWith("-"))
    {
        inputValue = input.toLongLong(&result);
    }
    else if (input.startsWith("0x"))
    {
        inputValue = input.remove("0x").toULongLong(&result, 16);
    }
    else if (input.startsWith("0b"))
    {
        inputValue = input.remove("0b").toULongLong(&result, 2);
    }
    else
    {
        inputValue = input.toULongLong(&result);
    }

    return result;

}

QVector<uint> BitDecoded::fieldStrToValues(const QString& field)
{
    // convert to std string
    std::string f = field.toStdString();

    std::set<uint32_t> s {0, m_maxNrOfBits};
    std::set<uint32_t> fieldsSet;
    if (!f.empty())
    {
        // convert the input field to vector format
        tokenize(f, fieldsSet);
    }

    if (!fieldsSet.empty())
    {
        s.insert(fieldsSet.begin(),fieldsSet.end());
    }
    else
    {
        // construct the default fields that increase by 4
        for (int i = 0; i < (int) m_maxNrOfBits ; i+=4)
        {
            s.insert(i);
        }
    }

    std::vector<uint32_t> v (s.begin(), s.end());

    return QVector<uint>::fromStdVector(v);
}

void BitDecoded::getBitField(const uint64_t value, const QVector<uint>&fields,
                            QVector<uint64_t>& bitField)
{
    uint64_t val = static_cast<qulonglong>(value);

    for (QVector<uint32_t>::const_iterator it = fields.begin();
         *it != m_maxNrOfBits; ++it)
    {
        uint64_t shift = *it;
        uint64_t mask = ((1ULL << (*(it+1) - *it)) -1) << shift;
        bitField.push_back((val & mask) >> shift);
    }
}

QString BitDecoded::updateResultString(const uint64_t val,
                          const QVector<uint>& fields,
                          const QVector<uint64_t>& bitField)
{
    std::string bit, base2, base10, result;


    const uint8_t NR_OF_BITS_PER_LINE =
                (m_windowWidth >= MAX_WINDOW_WIDTH) ? 32 : 16;
    const uint8_t WHITESPC_PER_BIT = 3;
    uint8_t nrOfWhitespace = WHITESPC_PER_BIT; // WHITESPC_PER_BIT * nrOfBits

    uint8_t n = 0;
    for (int i = 0; i < (int) m_maxNrOfBits ; ++i)
    {
        std::ostringstream s, s2, s10;
        s << std::setw(WHITESPC_PER_BIT) << i;

        std::string bit2 = (val & (1ULL << i)) ? "1":"0";

        if (std::find(fields.begin(), fields.end(), i + 1) != fields.end())
        {
            // assigned the string on the MSB of the field
            s2 << "|" << std::setw(WHITESPC_PER_BIT-1)<< bit2;
            s10 << "|" << std::setw(nrOfWhitespace-1) << bitField[n++];
            nrOfWhitespace = WHITESPC_PER_BIT;
        }
        else
        {
            s2 << std::setw(WHITESPC_PER_BIT)<< bit2;
            if (i % NR_OF_BITS_PER_LINE == (NR_OF_BITS_PER_LINE - 1))
            {
                // for the field across bit 32, print the empty string
                // and reset the nrOfWhitespace.
                s10 << std::setw(nrOfWhitespace)<< "";
                nrOfWhitespace = WHITESPC_PER_BIT;
            }
            else
            {
                nrOfWhitespace += WHITESPC_PER_BIT;
            }
        }

        bit.insert(0, s.str());
        base2.insert(0, s2.str());
        base10.insert(0, s10.str());

        // print the result at every 32 bits
        if (i % NR_OF_BITS_PER_LINE == (NR_OF_BITS_PER_LINE - 1))
        {
            bit.insert(0, "bit\t");
            base2.insert(0, "base2\t");
            base10.insert(0, "base10\t");

            result+= bit + "\n";
            result+= base2 + "\n";
            result+= base10+ "\n\n";

            bit.clear();
            base2.clear();
            base10.clear();
            nrOfWhitespace = WHITESPC_PER_BIT;
        }
    }

    return QString::fromStdString(result);
}

void BitDecoded::update()
{
    resetResults();
    qulonglong inValue = 0;
    if (! inputStrToValue(inValue, m_inputValue))
    {
        m_statusMsg = "Error: convert the input value failed.";
        return;
    }

    m_maxNrOfBits = (inValue > 0xFFFFFFFF) ? 64 : 32;

    m_inValDec = tr("Dec: %1 (%2)").arg(QString::number(inValue),
                                       QString::number(static_cast<qlonglong>(inValue)));
    m_inValHex = tr("Hex: 0x%1").arg(QString::number(inValue, 16));

    QVector<uint> field = fieldStrToValues(m_field);

    QVector<uint64_t> bitField;
    getBitField(inValue, field, bitField);

    m_output = updateResultString((int64_t) inValue, field, bitField);
}

void BitDecoded::resetResults()
{
    m_inValDec = "Dec:";
    m_inValHex = "Hex:";
    m_output.clear();
    m_statusMsg.clear();
}
