#ifndef BITDECODED_H
#define BITDECODED_H

#include <QObject>

class BitDecoded : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString inputValue READ inputValue WRITE setInputValue NOTIFY inputValueChanged)
    Q_PROPERTY(QString inputValueDec READ inputValueDec)
    Q_PROPERTY(QString inputValueHex READ inputValueHex)
    Q_PROPERTY(QString field READ field WRITE setField NOTIFY fieldChanged)
    Q_PROPERTY(QString output READ output NOTIFY inputValueChanged)
    Q_PROPERTY(QString statusMsg READ statusMsg NOTIFY statusMsgChanged)
    Q_PROPERTY(uint windowWidth READ windowWidth WRITE setWindowWidth NOTIFY windowWidthChanged)

public:
    explicit BitDecoded(QObject *parent = nullptr);

    QString inputValue();
    QString inputValueDec() { return m_inValDec; }
    QString inputValueHex() { return m_inValHex; }
    void setInputValue(const QString &inputValue);


    QString field() { return m_field; }
    void setField(const QString& inputField);

    QString output() { return m_output; }

    QString statusMsg() { return m_statusMsg; }

    uint windowWidth() { return m_windowWidth; }
    void setWindowWidth(const uint windowWidth);

    Q_INVOKABLE void pressButton();

signals:
    void inputValueChanged();
    void fieldChanged();
    void buttonPressed();
    void statusMsgChanged();
    void windowWidthChanged();

private:
    QString m_inputValue, m_inValDec, m_inValHex;
    QString m_field;
    QString m_output;
    QString m_statusMsg;
    uint m_windowWidth;
    uint m_maxNrOfBits;

    bool inputStrToValue(qulonglong& inputValue, QString input);
    void setInitOutput();


    // convert base
    QVector<uint> fieldStrToValues(const QString& fields);
    void getBitField(const uint64_t val, const QVector<uint>& fields, QVector<uint64_t>& bitField);
    QString updateResultString(const uint64_t val,
                              const QVector<uint>& fields,
                              const QVector<uint64_t>& bitField);

    void update();
    void resetResults();
};

#endif // BITDECODED_H
