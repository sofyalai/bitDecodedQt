import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3

import io.bitdecoded 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 520
    height: 500
    minimumWidth: 520
    minimumHeight: 500
    title: qsTr("BitDecoded")

    onWidthChanged:
    {
        bitdecoded.windowWidth = window.width
        updateData();
    }

    BitDecoded {
        id: bitdecoded
        windowWidth: window.width
    }

    Label {
        id: label
        width: 75
        height: 40
        text: qsTr("Input Value")
        anchors.top: fieldLabel.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        verticalAlignment: Text.AlignVCenter
    }

    Label {
        id: fieldLabel
        width: 75
        height: 40
        text: qsTr("Field")
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        verticalAlignment: Text.AlignVCenter
    }

    Label {
        id: inputValueLabelDec
        x: 21
        width: 230
        height: 37
        text: qsTr("Dec: ")
        anchors.top: label.bottom
        anchors.topMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        verticalAlignment: Text.AlignVCenter
    }

    Label {
        id: inputValueLabelHex
        y: 206
        width: 230
        height: 37
        text: qsTr("Hex: ")
        anchors.verticalCenter: inputValueLabelDec.verticalCenter
        anchors.left: inputValueLabelDec.right
        anchors.leftMargin: 20
        verticalAlignment: Text.AlignVCenter
    }

    menuBar: MenuBar {
        Menu {
            title: "Help"
            MenuItem {
                text: "About"
                onTriggered: {
                    dialogAbout.open()
                }
            }
        }
    }

    Dialog {
        id: dialogAbout
        modal: true
        title: "About"

        width: 400
        height:350
        x: (parent.width -width)/2
        y: (parent.height- height) /2

        footer: Flow {
            spacing: 5
            width: parent.width
            leftPadding: 20
            bottomPadding: 20

            Button {
                id: buttonLicense
                checkable: true
                width:parent.width /5
                text: "License"
                onPressed:
                {
                    labelAboutExtra.text = "This program is free software; you can redistribute it and/or modify it "
                            + "under the terms of the GNU General Public License as published by the Free "
                            + "Software Foundation; either version 2 of the License, or (at your option) "
                            + "any later version.\n\n"
                            + "This program is distributed in the hope that it will be useful, but WITHOUT "
                            + "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
                            + "FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for"
                            + "more details.\n\n"
                            + "You should have received a copy of the GNU General Public License along with "
                            + "this program; if not, write to the Free Software Foundation, Inc., 51 Franklin "
                            + "Street, Fifth Floor, Boston, MA 02110-1301, USA."
                    labelAboutExtra.visible = checked ? false : true
                    buttonCredit.checked = false
                }
            }

            Button {
                id: buttonCredit
                checkable: true
                width:parent.width /5
                text: "Credit"
                onPressed:
                {
                    labelAboutExtra.text = "Created by\n"
                            + "Ying-Chun Lai Nyqvist  <yclai017@gmail.com>"
                    labelAboutExtra.visible = checked ? false : true
                    buttonLicense.checked = false
                }
            }

            Button {
                width:parent.width /5
                opacity: 0
            }

            Button {
                width:parent.width /5
                text: "Close"
                onPressed: dialogAbout.close()
            }
        }

        Flickable {
            id: flickable
            clip: true
            anchors.fill: parent
            contentHeight: column.height

            Column {
                id: column
                width: parent.width
                spacing: 20
                Label {
                    id: labelAboutTitle
                    width: parent.width
                    text: "BitDecoded 1.0 Alpha"
                    horizontalAlignment: Text.AlignHCenter
                    font.bold: true
                    font.pointSize: 14
                }

                Label {
                    id: labelAbout
                    width: parent.width
                    text: "BitDecoded is a tool to present a value\n"
                          +"in different numeric-based format\n"
                          +"with user-defined fields."
                    horizontalAlignment: Text.AlignHCenter
                }

                ScrollView {
                    width: parent.width
                    height: 100
                    TextArea {
                        id: labelAboutExtra
                        width: parent.width
                        horizontalAlignment: Text.AlignLeft
                        visible: false
                        wrapMode: TextEdit.Wrap
                    }
                }
            }
        }

    }

    function updateData() {
        statusLabel.text = bitdecoded.statusMsg
        inputValueLabelDec.text = bitdecoded.inputValueDec
        inputValueLabelHex.text = bitdecoded.inputValueHex
        textArea.text = bitdecoded.output

    }

    Button {
        id: button
        width: 80
        text: qsTr("Enter")
        anchors.verticalCenterOffset: 0
        anchors.verticalCenter: textField_inValue.verticalCenter
        anchors.left: textField_inValue.right
        anchors.leftMargin: 10
        padding: 6
        onClicked:
        {
            bitdecoded.pressButton()
            updateData()
        }
    }

    TextField {
        id: textField_inValue
        width: 300
        height: 40
        text: bitdecoded.inputValue
        anchors.left: label.right
        anchors.leftMargin: 20
        clip: true
        anchors.top: textField_field.bottom
        anchors.topMargin: 20
        placeholderText: qsTr("Default: 0")
        onEditingFinished:
        {
            bitdecoded.inputValue = text
            updateData()
        }
    }

    TextField {
        id: textField_field
        width: 300
        height: 40
        text: bitdecoded.field
        anchors.left: fieldLabel.right
        anchors.leftMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20
        placeholderText: qsTr("(Optional) Default: 0,4,8,16...64")
        onEditingFinished:
        {
            bitdecoded.field = text
            updateData()
        }
    }

    TextArea {
        id: textArea
        height: 250
        text: bitdecoded.output
        anchors.top: inputValueLabelDec.bottom
        anchors.topMargin: 2
        anchors.right: parent.right
        anchors.rightMargin: 10
        font.pointSize: 10
        font.family: "Monospace"
        anchors.left: parent.left
        anchors.leftMargin: 10
        readOnly: true
    }


    footer: Label {
        id: statusLabel
        height: 24
        color: "#7d021c"
        anchors.left: parent.left
        anchors.leftMargin: 15
        styleColor: "#000000"
        anchors.right: parent.right
        anchors.rightMargin: 25
        verticalAlignment: Text.AlignVCenter
    }

}
