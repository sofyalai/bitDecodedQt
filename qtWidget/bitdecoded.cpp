#include "bitdecoded.h"
#include "ui_bitdecoded.h"

#include <vector>
#include <set>
#include <sstream>
#include <iomanip>
#include <algorithm>

#include <QDebug>
#include <QFormLayout>

#define MAX_NR_OF_BITS  64
#define BIT_32          32

#define SHOW_BIT_FILEDS_WIDTH 1000
#define DEFAULT_WIDTH 760

using namespace std;

bitDecoded::bitDecoded(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::bitDecoded),
    displayAbout(new DisplayAbout()),
    isBitFieldInputShowed(false)
{
    ui->setupUi(this);

    initGridLayout();

    ui->scrollArea->setLayout(ui->formLayout);
    ui->scrollAreaWidgetContents->setLayout(ui->formLayout);

    connect(ui->convertButton, SIGNAL(released()), this, SLOT(handleConvertButton()));
    connect(ui->inputLineEdit, SIGNAL(returnPressed()), this, SLOT(handleConvertButton()));
    connect(ui->fieldLineEdit, SIGNAL(returnPressed()), this, SLOT(handleConvertButton()));
    connect(ui->bitFieldsButton, SIGNAL(released()), this, SLOT(showBitFiledsInput()));

    // Menu related signals
    connect(ui->actionAbout_BitDecoded, SIGNAL(triggered()), this, SLOT(showAbout()));
}

bitDecoded::~bitDecoded()
{
    delete ui;
    ui = NULL;
    delete displayAbout;
    displayAbout = NULL;
}

void bitDecoded::initGridLayout()
{
    // set the field
    QVector<uint> fieldV = fieldStrToValues(ui->fieldLineEdit->text());

    // get bitField value in the every field
    vector<uint64_t> bitField;
    qulonglong initVal = 0;
    getBitField(initVal, fieldV, bitField);

    // get the string buffer for the print out
    (void) updateResultString(initVal, fieldV, bitField);

}

bool bitDecoded::inputStrToValue(qulonglong& inputValue, QString input)
{
    bool result = true;

    if (input.isEmpty())
    {
        return false;
    }

    if (input.startsWith("-"))
    {
        inputValue = input.toLongLong(&result);
    }
    else if (input.startsWith("0x"))
    {
        inputValue = input.remove("0x").toULongLong(&result, 16);
    }
    else if (input.startsWith("0b"))
    {
        inputValue = input.remove("0b").toULongLong(&result, 2);
    }
    else
    {
        inputValue = input.toULongLong(&result);
    }

    return result;

}

void bitDecoded::handleConvertButton()
{
    ui->statusBar->clearMessage();
    // set the inputValue
    qulonglong inputValue = 0;
    if (!inputStrToValue(inputValue, ui->inputLineEdit->text()))
    {
        ui->statusBar->showMessage("ERROR: cannot convert the value.");
        return;
    }

    // set the field
    QVector<uint> fieldV = fieldStrToValues(ui->fieldLineEdit->text());

    // get bitField value in the every field
    vector<uint64_t> bitField;
    getBitField(inputValue, fieldV, bitField);

    // print hex format
    QString hexFormat(tr("HEX: 0x%1").arg(QString::number(inputValue, 16)));
    ui->hexLabel->setText(hexFormat);

    // print dec format
    QString decFormat(tr("DEC: %1 (%2)").arg(QString::number(inputValue),
                                             QString::number(static_cast<qlonglong>(inputValue))));
    ui->decLabel->setText(decFormat);

    // print out the result
    (void) updateResultString(inputValue, fieldV, bitField);
}

inline void tokenize(const std::string& s, std::set<uint32_t>& sValue)
{
    const char* delimiters = " ,";
    char* data = strtok((char*) s.c_str(), delimiters);
    int i = 0;
    while(data != NULL)
    {
        sValue.insert(strtoul(data, NULL, 0));
        data = strtok(NULL, delimiters);
        ++i;
    }
}

std::vector<uint32_t> bitDecoded::fieldStrToValues(const std::string& fields)
{
    std::set<uint32_t> s {0, MAX_NR_OF_BITS};
    std::set<uint32_t> fieldsSet;
    if (!fields.empty())
    {
        // convert the input field to vector format
        tokenize(fields, fieldsSet);
    }

    if (!fieldsSet.empty())
    {
        s.insert(fieldsSet.begin(),fieldsSet.end());
    }
    else
    {
        // construct the default fields that increase by 4
        for (int i = 0; i < MAX_NR_OF_BITS ; i+=4)
        {
            s.insert(i);
        }
    }

    std::vector<uint32_t> v (s.begin(), s.end());

    return v;
}

QVector<uint> bitDecoded::fieldStrToValues(const QString& fields)
{
    std::vector<uint32_t> v = fieldStrToValues(fields.toStdString());
    return QVector<uint>::fromStdVector(v);
}

string bitDecoded::updateResultString(const uint64_t val,
                                      const vector<uint32_t>& fields,
                                      const vector<uint64_t>& bitField)
{
    string bit, base2, base10, result;

    const unsigned int WHITESPC_PER_BIT = 3;
    unsigned int nrOfWhitespace = WHITESPC_PER_BIT; // WHITESPC_PER_BIT * nrOfBits

    uint8_t n = 0;
    for (unsigned int i = 0; i < MAX_NR_OF_BITS ; ++i)
    {
        ostringstream s, s2, s10;
        s << setw(WHITESPC_PER_BIT) << i;

        string bit2 = (val & (1ULL << i)) ? "1":"0";

        if (std::find(fields.begin(), fields.end(), i + 1) != fields.end())
        {
            // assigned the string on the MSB of the field
            s2 << "|" << setw(WHITESPC_PER_BIT-1)<< bit2;
            s10 << "|" << setw(nrOfWhitespace-1) << bitField[n++];
            nrOfWhitespace = WHITESPC_PER_BIT;
        }
        else
        {
            s2 << setw(WHITESPC_PER_BIT)<< bit2;
            if (i % BIT_32 == (BIT_32 - 1))
            {
                // for the field across bit 32, print the empty string
                // and reset the nrOfWhitespace.
                s10 << setw(nrOfWhitespace)<< "";
                nrOfWhitespace = WHITESPC_PER_BIT;
            }
            else
            {
                nrOfWhitespace += WHITESPC_PER_BIT;
            }
        }

        bit.insert(0, s.str());
        base2.insert(0, s2.str());
        base10.insert(0, s10.str());

        // print the result at every 32 bits
        if (i % BIT_32 == (BIT_32 - 1))
        {
            bit.insert(0, "bit\t");
            base2.insert(0, "base2\t");
            base10.insert(0, "base10\t");

            // print the output to labels.
            if ( i < BIT_32)
            {
                ui->bit_0->setText(QString::fromStdString(bit));
                ui->base2_0->setText(QString::fromStdString(base2));
                ui->base10_0->setText(QString::fromStdString(base10));
            }
            else
            {
                ui->bit_1->setText(QString::fromStdString(bit));
                ui->base2_1->setText(QString::fromStdString(base2));
                ui->base10_1->setText(QString::fromStdString(base10));
            }

            result+= bit + "\n";
            result+= base2 + "\n";
            result+= base10+ "\n";

            bit.clear();
            base2.clear();
            base10.clear();
            nrOfWhitespace = WHITESPC_PER_BIT;
        }
    }
    return result;
}

void bitDecoded::getBitField(uint64_t val, const vector<uint32_t>& fields, vector<uint64_t>& bitField)
{
    for (vector<uint32_t>::const_iterator it = fields.begin();
         *it != MAX_NR_OF_BITS; ++it)
    {
        uint64_t shift = *it;
        uint64_t mask = ((1ULL << (*(it+1) - *it)) -1) << shift;
        bitField.push_back((val & mask) >> shift);
    }
}

void bitDecoded::showBitFiledsInput()
{
    if (isBitFieldInputShowed)
    {
        this->resize(DEFAULT_WIDTH, 310);
        removeBitFieldPanel();
        ui->bitFieldsButton->setText(">>");
        isBitFieldInputShowed = false;
    }
    else
    {
        this->resize(SHOW_BIT_FILEDS_WIDTH, 310);
        constructBitFieldsPanel();
        ui->bitFieldsButton->setText("<<");
        isBitFieldInputShowed = true;
    }
}

void bitDecoded::constructBitFieldsPanel()
{
    QVector<uint> fieldV = fieldStrToValues(ui->fieldLineEdit->text());
    for (QVector<uint>::const_iterator it = fieldV.begin();
         *it != MAX_NR_OF_BITS; ++it)
    {
        QLineEdit* edit = new QLineEdit();
        bitFieldValues.push_back(edit);
        ui->formLayout->addRow(tr("[%1..%2] ").arg(QString::number(*it),
                                                   QString::number(*(it+1)-1)), edit);

    }
}

void bitDecoded::removeBitFieldPanel()
{
    int ROW_COUNT = ui->formLayout->rowCount();
    while(ROW_COUNT--)
    {
        ui->formLayout->removeRow(0);
    }

    bitFieldValues.clear();
}
