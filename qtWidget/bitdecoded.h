#ifndef BITDECODED_H
#define BITDECODED_H

#include <QMainWindow>
#include "displayabout.h"

class Vector;
class QLineEdit;

namespace Ui {
    class bitDecoded;
}

class bitDecoded : public QMainWindow
{
    Q_OBJECT

public:
    explicit bitDecoded(QWidget *parent = 0);
    ~bitDecoded();

    private slots:
        void handleConvertButton();
        void showBitFiledsInput();
        // menu actions
        void showAbout();

private:
        Ui::bitDecoded *ui;
        DisplayAbout* displayAbout;
        bool isBitFieldInputShowed;

        QVector<QLineEdit*> bitFieldValues;

        void initGridLayout();
        bool inputStrToValue(qulonglong& inputValue, QString input);
        std::vector<uint32_t> fieldStrToValues(const std::string& fields);
        QVector<uint> fieldStrToValues(const QString& fields);

        std::string updateResultString(const uint64_t val,
                                       const std::vector<uint32_t>& fields,
                                       const std::vector<uint64_t>& bitField);

        QString updateResultString(const qulonglong val,
                                   const QVector<uint>& fields,
                                   const std::vector<uint64_t>& bitField)
        {
            return QString::fromStdString(updateResultString(
                                              (uint64_t) val,
                                              fields.toStdVector(),
                                              bitField));
        }

        void getBitField(uint64_t val, const std::vector<uint32_t>& fields,
                         std::vector<uint64_t>& bitField);
        void getBitField(const qulonglong val, const QVector<uint>&fields,
                         std::vector<uint64_t>& bitField)
        {
            getBitField((qlonglong) val, fields.toStdVector(), bitField);
        }

        void constructBitFieldsPanel();
        void removeBitFieldPanel();
};

#endif // BITDECODED_H
