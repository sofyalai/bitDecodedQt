#include "displayabout.h"
#include "ui_displayabout.h"

#include <QDebug>

#define LICENSE_PRINT "\
This program is free software; you can redistribute it and/or modify it\
under the terms of the GNU General Public License as published by the Free\
Software Foundation; either version 2 of the License, or (at your option)\
any later version.\n\n\
This program is distributed in the hope that it will be useful, but WITHOUT\
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or\
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for\
more details.\n\n\
You should have received a copy of the GNU General Public License along with\
this program; if not, write to the Free Software Foundation, Inc., 51 Franklin\
Street, Fifth Floor, Boston, MA 02110-1301, USA."

#define CREDIT_PRINT "\
Created by\n\
Ying-Chun Lai Nyqvist  <yclai017@gmail.com>"

#define EXTRA_DISPLAY_HEIGHT    450
#define DEFAULT_DISPLAY_HEIGHT  300
#define DEFAULT_DISPLAY_WIDTH   400

#define VERSION "1.0.0"
#define RELEASE_YEAR "2017"

DisplayAbout::DisplayAbout(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DisplayAbout),
    isCreditsShowed(false),
    isLicenseShowed(false)
{
    ui->setupUi(this);  
    connect(ui->pushButton, SIGNAL(released()), this, SLOT(showAboutLicense()));
    connect(ui->pushButton_2, SIGNAL(released()), this, SLOT(showAboutCredits()));
}

DisplayAbout::~DisplayAbout()
{
    delete ui;
}

void DisplayAbout::setDescription(const QString& text)
{
    ui->textBrowser->setText(text);
}

void DisplayAbout::showAboutLicense()
{
    isCreditsShowed = false;
    ui->textBrowser_2->setText(LICENSE_PRINT);

    if (isLicenseShowed)
    {
        // hide the license
        this->resize(DEFAULT_DISPLAY_WIDTH, DEFAULT_DISPLAY_HEIGHT);
        isLicenseShowed = false;
    }
    else
    {
        // show the license
        this->resize(DEFAULT_DISPLAY_WIDTH, EXTRA_DISPLAY_HEIGHT);
        isLicenseShowed = true;
    }
}

void DisplayAbout::showAboutCredits()
{
    isLicenseShowed = false;
    ui->textBrowser_2->setText(CREDIT_PRINT);

    if (isCreditsShowed)
    {
        // hide the credits
        this->resize(DEFAULT_DISPLAY_WIDTH, DEFAULT_DISPLAY_HEIGHT);
        isCreditsShowed = false;
    }
    else
    {
        // show the credits
        this->resize(DEFAULT_DISPLAY_WIDTH, EXTRA_DISPLAY_HEIGHT);
        isCreditsShowed = true;
    }
}
