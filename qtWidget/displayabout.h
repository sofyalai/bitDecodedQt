#ifndef DISPLAY_ABOUT_H
#define DISPLAY_ABOUT_H

#include <QWidget>

namespace Ui {
class DisplayAbout;
}

class DisplayAbout : public QWidget
{
    Q_OBJECT

public:
    explicit DisplayAbout(QWidget *parent = 0);
    ~DisplayAbout();
    void setDescription(const QString& text);

private:
    Ui::DisplayAbout *ui;
    bool isCreditsShowed;
    bool isLicenseShowed;

private slots:
    void showAboutLicense();
    void showAboutCredits();
};

#endif // DISPLAY_ABOUT_H
